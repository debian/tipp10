/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Implementation of the StatusBar class
** File name: statusbar.cpp
**
****************************************************************/

#include <QColor>
#include <QFont>
#include <QPainter>

#include "def/defines.h"
#include "errormessage.h"
#include "statusbar.h"

StatusBar::StatusBar(QWidget* parent)
    : QWidget(parent)
    , txtLeftLeft("")
    , txtLeft("")
    , txtCenter("")
    , txtRight("")
    , txtRightRight("")
{
    // Load background image
    if (!background.load(":/img/statusbg.png")) {
        // Error message
        ErrorMessage* errorMessage = new ErrorMessage(this);
        errorMessage->showMessage(Error::status_pic,
            ErrorMessage::Type::Warning, ErrorMessage::Cancel::Operation);
    }

    setFixedSize(610, 30);

    setAttribute(Qt::WA_NoSystemBackground);
}

void StatusBar::setText(QString leftleft, QString left, QString center,
    QString right, QString rightright)
{
    txtLeftLeft = leftleft;
    txtLeft = left;
    txtCenter = center;
    txtRight = right;
    txtRightRight = rightright;
    repaint();
}

void StatusBar::setLeftLeftText(QString txt)
{
    txtLeftLeft = txt;
    repaint();
}

void StatusBar::setLeftText(QString txt)
{
    txtLeft = txt;
    repaint();
}

void StatusBar::setCenterText(QString txt)
{
    txtCenter = txt;
    repaint();
}

void StatusBar::setRightText(QString txt)
{
    txtRight = txt;
    repaint();
}

void StatusBar::setRightRightText(QString txt)
{
    txtRightRight = txt;
    repaint();
}

void StatusBar::paintEvent([[maybe_unused]] QPaintEvent* pevent)
{
    QPainter painter(this);
    // Draw backgorund image
    painter.drawPixmap(0, 0, background);
    painter.setFont(QFont(t10::font_standard, t10::font_size_status));
    painter.setPen(QColor(40, 40, 40));
    painter.drawText(
        190, 7, 230, 14, Qt::AlignCenter | Qt::AlignVCenter, txtCenter);
    painter.setPen(QColor(120, 120, 120));
    painter.drawText(
        10, 7, 80, 14, Qt::AlignCenter | Qt::AlignVCenter, txtLeftLeft);
    painter.drawText(
        100, 7, 80, 14, Qt::AlignCenter | Qt::AlignVCenter, txtLeft);
    painter.drawText(
        430, 7, 80, 14, Qt::AlignCenter | Qt::AlignVCenter, txtRight);
    painter.drawText(
        520, 7, 80, 14, Qt::AlignCenter | Qt::AlignVCenter, txtRightRight);
}
